function* fibonacci_generator(n,current=0,next=1){
    if(n===0){
        return n;
    }
    else{
        yield current;
        yield* fibonacci_generator(n-1,next,current+next);
    }
}
fibo=fibonacci_generator(2);
console.log(fibo.next());
console.log(fibo.next());
console.log(fibo.next());